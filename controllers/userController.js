'use strict'

var User = require('../models/user');
var Follow = require('../models/follow');
var Publication = require('../models/publication');

var bcrypt = require('bcrypt-nodejs');
var jwt = require('../services/jwt');
var mongoosePaginate = require('mongoose-pagination');

//para trabajar con ficheros:
var fs = require('fs');
var path = require('path')




function pruebas(req, res) {
    console.log(req.body);
    res.status(200).send({
        message: "Acción de prueba en el servidor de NodeJS"
    });
}

//registro de usuarios
function saveUser(req, res) {
    var params = req.body;
    var user = new User();
    if (params.name && params.surname && params.nick && params.email && params.password) {
        user.name = params.name;
        user.surname = params.surname;
        user.nick = params.nick;
        user.email = params.email;
        user.role = 'ROLE_USER';
        user.image = null;

        //Controlar usuarios duplicados
        var nickLower = user.nick.toLowerCase();
        User.find({
            $or: [
                { nick: user.nick.toLowerCase() },
                { email: user.email.toLowerCase() }
            ]
        }).exec((err, users) => {
            if (err) return res.status(500).send({ message: 'Error en la petición de usuario' });
            if (users && users.length >= 1) {
                return res.status(200).send({ message: 'El usuario que intentas registrar ya existe!' });
            } else {

                //Cifra la password y guarda el nuevo usurario
                bcrypt.hash(params.password, null, null, (error, result) => {
                    user.password = result;
                    user.save((err, userStored) => {
                        if (err) return res.status(500).send({ message: 'Error, no se ha podido guardar el usuario' });
                        if (userStored) {
                            res.status(200).send({ user: userStored });
                        } else {
                            res.status(404).send({ message: 'No se ha registrado el usuario ' });
                        }
                    });
                }
                );


            }
        });



    } else {
        res.status(200).send({
            message: 'Envía todos los datos necesarios',
        })
    }

}

//login de usuario
function loginUser(req, res) {
    var params = req.body;
    var email = params.email;
    var password = params.password;

    User.findOne({ email: email }, (err, user) => {
        if (err) return res.status(500).send({ message: 'El usuario no se ha podido encontrar' });

        if (user) {
            bcrypt.compare(password, user.password, (err, check) => {
                if (check) {
                    if (params.getToken) {
                        return res.status(200).send({ token: jwt.createToken(user) });
                    } else {
                        user.password.undefined;
                        return res.status(200).send({ user });
                    }
                } else {
                    return res.status(404).send({ message: 'La contraseña es incorrecta' });
                }

            });
        } else {
            return res.status(404).send({ message: ' El usuario no existe' });
        }

    });

}

//Conseguir datos de usuario:
function getUser(req, res) {

    //recoger id de usuario que viene por URL(req.params)
    var userId = req.params.id;

    User.findById(userId, (err, user) => {
        if (err) return res.status(500).send({ message: 'Error en la petición' });
        if (!user) return res.status(404).send({ message: 'El usuario no existe' });

        followThisUser(req.user.sub, userId).then((value) => {
            return res.status(200).send({
                user: user,
                following: value.following,
                followed: value.following,
            });
        });


    });

}

async function followThisUser(identity_user_id, user_id) {
    try {
        var following = await Follow.findOne({ "user": identity_user_id, "followed": user_id }).exec()
            .then((following) => {
                return following;
            })
            .catch((error) => {
                return handleError(error);
            })


        var followed = await Follow.findOne({ "user": user_id, "followed": identity_user_id }).exec()
            .then((followed) => {
                return followed;
            })
            .catch((error) => {
                return handleError(error);
            });

        return {
            following: following,
            followed: followed,
        }
    } catch (e) {
        console.log(e);
    }

}


function getCounters(req, res) {
    var userId = req.user.sub;
    if (req.params.id) {
        userId = req.params.id;
    }

    getCountFollow(userId).then((value) => {
        return res.status(200).send(value);
    });

}

async function getCountFollow(user_id) {
    try {
        var following = await Follow.count({ "user": user_id }).exec()
            .then((count) => {
                return count;
            })
            .catch((error) => {
                return handleError(error);
            });

        var followed = await Follow.count({ "followed": user_id }).exec()
            .then((count) => {
                return count;
            })
            .catch((error) => {
                return handleError(error);
            });

        var publications = await Publication.count({ "user": user_id }).exec()
            .then((count) => {
                return count;
            })
            .catch((error) => {
                return handleError(error);
            });

        return {
            following: following,
            followed: followed,
            publications: publications,
        }

    } catch (e) {
        console.log(e);
    }
}

//Devolver listado de usuario paginados

function getUsers(req, res) {
    var identity_user_id = req.user.sub;

    var page = 1;
    if (req.params.page) {
        page = req.params.page
    }

    var itemsPerPage = 5;

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        if (err) return res.status(500).send({ message: 'Error en la petición' });
        if (!users) return res.status(404).send({ message: 'No hay usuarios disponibles' });

        followUserId(identity_user_id).then((value) => {

            return res.status(200).send({
                users,
                users_following: value.following,
                user_followed: value.followed,
                total,
                pages: Math.ceil(total / itemsPerPage)
            });


        });


    });
}

async function followUserId(user_id) {
    try {
        var following = await Follow.find({ "user": user_id }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec()
            .then((follows) => {

                var follows_clean = [];
                follows.forEach((follow) => {
                    follows_clean.push(follow.followed);
                });
                console.log(follows_clean);
                return follows_clean;

            })
            .catch((error) => {
                return handleError(error);
            });

        var followed = await Follow.find({ "user": user_id }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec()
            .then((follows) => {
                var follows_clean = [];
                follows.forEach((follow) => {
                    follows_clean.push(follow.user);
                });
                return follows_clean;

            })
            .catch((error) => {
                return handleError(error);
            });

        return {
            following: following,
            followed: followed,
        }
    } catch (e) {
        console.log(e);
    }


}

//actualizar usuarios
function updateUsers(req, res) {
    var userId = req.params.id;
    var update = req.body;

    //borrar propiedad password
    delete update.password;

    if (userId != req.user.sub) {
        return res.status(500).send({ message: 'No tienes permiso para actualizar el usuario' });
    }

    User.find({
        $or: [
            { nick: update.nick },
            { email: update.email },
        ]
    }).exec((err, users) => {
        if (err) return res.status(500).send({ message: 'Error en la petición' });

        let user_isset = false;
        users.forEach(user => {
            if (user && user._id != userId) {
                user_isset = true;
            }
        });
        if (user_isset == false) {
            User.findByIdAndUpdate(userId, update, { new: true }, (err, userUpdated) => {
                if (err) return res.status(500).send({ message: 'Error en la petición' });

                if (!userUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' });

                return res.status(200).send({ user: userUpdated });
            });

        } else {
            return res.status(404).send({ message: 'Ya existe el nick' });
        }

    });


}

//subir avatar de usuarios

function uploadImage(req, res) {
    //recojo el id de usuario mandadado por la url
    var userId = req.params.id;

    //compruebo que el usuario mandado es igual al de la request (el que está logueado)
    if (userId != req.user.sub) {
        fs.unlink(file_path, (error) => {
            return res.status(500).send({ message: 'No tienes permiso para actualizar el usuario' });
        });

    }

    //si se sube un archivo:
    if (req.files.image) {
        let file_path = req.files.image.path;
        let file_split = file_path.split('\\');
        //console.log(file_split);
        let image_name = file_split[2];
        //console.log(image_name);
        let image_name_split = image_name.split('\.');
        //console.log(image_name_split);
        let image_extension = image_name_split[1];
        console.log(image_extension);
        if (image_extension == 'jpg' || image_extension == 'jpeg' || image_extension == 'gif' || image_extension == 'png') {
            User.findByIdAndUpdate(userId, { image: image_name }, { new: true }, (err, userUpdated) => {
                if (err) return res.status(500).send({ message: 'Error en la petición' });

                if (!userUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' });

                return res.status(200).send({ user: userUpdated });
            });
        } else {
            fs.unlink(file_path, (error) => {
                return res.status(500).send({ message: 'Extensión no válida' });
            });
        }
    } else {
        return res.status(404).send({ message: 'no se ha subido una imagen' });
    }
}

function getImageFile(req, res) {
    let image_file = req.params.imageFile;
    let path_file = './uploads/users/' + image_file;

    fs.exists(path_file, (exists) => {
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            res.status(200).send({ message: 'No existe la imagen' });
        }
    });

}




module.exports = {
    pruebas,
    saveUser,
    loginUser,
    getUser,
    getCounters,
    getUsers,
    updateUsers,
    uploadImage,
    getImageFile,
}