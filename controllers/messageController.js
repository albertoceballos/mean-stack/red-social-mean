'use strict'

var moment=require('moment');
var mongoose_pagination=require('mongoose-pagination');

//modelos
var User=require('../models/user');
var Follow=require('../models/follow');
var Message=require('../models/message');


function saveMessage(req,res){
    var params=req.body;
    if(!params.text || !params.receiver){
        return res.status(200).send({message: 'Envía los datos necesaios'});
    }

    var message=new Message();

    message.text=params.text;
    message.receiver=params.receiver;
    message.emitter=req.user.sub;
    message.created_at=moment().unix();
    message.viewed=false;

    message.save((err,messageStored)=>{
        if(err) return res.status(500).send({message: 'Error de servidor al enviar mensaje'});
        if(!messageStored) return res.status(404).send({message:'Error al enviar el mensaje'});
        
        return res.status(200).send({message:messageStored});

    });

}

function getReceivedMessages(req,res){
    var userId=req.user.sub;
    var page=1;

    if(req.params.page){
        page=req.params.page;
    }
    var itemsPerPage=4;

    Message.find({'receiver':userId}).populate('emitter receiver').sort('-created_at').paginate(page,itemsPerPage,(err,messages,total)=>{
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!messages) return res.status(404).send({message:'No hay mensajes'});

        return res.status(200).send({
            total:total,
            page:page,
            pages:Math.ceil(total/itemsPerPage),
            messages,
        });
    });

}

function getEmittedMessages(req,res){
    var userId=req.user.sub;
    var page=1;

    if(req.params.page){
        page=req.params.page;
    }
    var itemsPerPage=4;

    Message.find({'emitter':userId}).populate('emitter receiver').sort('-created_at').paginate(page,itemsPerPage,(err,messages,total)=>{
        if(err) return res.status(500).send({message: 'Error en la petición'});

        if(!messages) return res.status(404).send({message:'No hay mensajes'});

        return res.status(200).send({
            total:total,
            page:page,
            pages:Math.ceil(total/itemsPerPage),
            messages,
        });
    });

}

function getUnviewedMessages(req,res){
    var userId=req.user.sub;

    Message.countDocuments({'receiver':userId,'viewed':false}).exec((err,result)=>{
        if(err) return res.status(500).send({message:'Error en la petición'});

        return res.status(200).send({
            unviewed:result,
        })

    });
}

function setViewedMessages(req,res){
    var userId=req.user.sub;

    Message.update({receiver:userId, viewed:false},{'viewed':true}, {multi:true},(err,messagesUpdate)=>{
        if(err) return res.status(500).send({message:'Error en la petición'});

        return res.status(200).send({
            messages:messagesUpdate,
        });
    });

}

function deleteMessage(req,res){
    var messageId=req.params.id;
    var userId=req.user.sub;

    Message.findOneAndRemove({'receiver':userId,'_id':messageId},(err,messageRemoved)=>{
        if(err) return res.status(500).send({message:'Eror al borrar el mensaje'});
        if(!messageRemoved) return res.status(404).send({message:'El mensaje no existe'});
        return res.status(200).send({message:'Mensaje borrado con éxito'});
    });

}


module.exports={
    saveMessage,
    getReceivedMessages,
    getEmittedMessages,
    getUnviewedMessages,
    setViewedMessages,
    deleteMessage,
}