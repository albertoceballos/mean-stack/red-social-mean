'use strict'

var express=require('express');
var FollowController=require('../controllers/followController');

var middleware_auth=require('../middlewares/authenticated');

var api=express.Router();

api.post('/follow',middleware_auth.ensureAuth,FollowController.saveFollow);
api.delete('/delete-follow/:id',middleware_auth.ensureAuth,FollowController.deleteFollow);
api.get('/following/:id?/:page?',middleware_auth.ensureAuth, FollowController.getFollowingUsers);
api.get('/followed/:id?/:page?',middleware_auth.ensureAuth,FollowController.getFollowedUsers);
api.get('/get-my-follows/:followed?',middleware_auth.ensureAuth,FollowController.getMyFollows);



module.exports=api;
